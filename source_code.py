from machine import Pin, I2C, ADC, Timer, PWM
from ssd1306 import SSD1306_I2C
from urtc import DS1307

import utime
#########################################

# constants
#OLED
WIDTH = 128
HEIGHT = 64

# SOIL MOISTER
WET = 10832
DRY = 22180

# ILUMINATION
DARK = 1100
LIGHT = 64000

#########################################

# global variables
CURRENT_HUMIDITY = 0

CURRENT_ILUMINATION = 0

TIME_H = 0
TIME_M = 0

HOURS_ON = 17
MINUTES_ON = 20

HOURS_OFF = 20
MINUTES_OFF = 30

DUTY = 0
MAX_DUTY = 60000
MIN_DUTY = 0

CURSOR = 0
CHANGE_DATA = False

VALUE_HAS_CHANGED = False

#########################################

# init pheriphery
adc_soil = ADC(0)
adc_ilum = ADC(1)

#########################################

# functions

def get_humidity(soil):
    global DRY, WET, CURRENT_HUMIDITY
    tmp_sensor = adc_soil.read_u16()
    CURRENT_HUMIDITY = int((tmp_sensor - DRY) * (100 - 0) / (WET - DRY) + 0)

def get_ilumination(ilum):
    global DARK, LIGHT, CURRENT_ILUMINATION
    tmp_ilum = adc_ilum.read_u16()
    CURRENT_ILUMINATION = int((tmp_ilum - DARK) * (100 - 0) / (LIGHT - DARK) + 0)

def show_on_display(oled):
    global HOURS_ON, MINUTES_ON, HOURS_OFF, MINUTES_OFF, CURRENT_HUMIDITY, TIME_H, TIME_M
    oled.fill(0)
    oled.text(str(TIME_H) +":" + str(TIME_M), 43, 5)
    oled.text("LED", 52, 18)
    oled.text(str(HOURS_ON) + ":" + str(MINUTES_ON), 12, 28)
    oled.text(" - ", 52, 28)
    oled.text(str(HOURS_OFF) + ":" + str(MINUTES_OFF), 75, 28)
    oled.text("Vlhkost: " + str(CURRENT_HUMIDITY) + "%", 5, 43)
    oled.text("Osvetleni: " + str(CURRENT_ILUMINATION) + "%", 5, 53)
    oled.show()
    
def show_cursor(CURSOR, oled):
    print("i am in show cursor function")
    if CURSOR == 0:
        oled.text(">", 5, 28)
        oled.text(" ", 52, 28)
        oled.text(" ", 52, 28)
        oled.text(" ", 110, 48)
    elif CURSOR == 1:
        oled.text("<", 52, 28)
        oled.text(" ", 5, 28)
        oled.text(" ", 110, 48)
    elif CURSOR == 2:
        oled.text(">", 68, 28)
        oled.text(" ", 5, 28)
        oled.text(" ", 110, 48)
    elif CURSOR == 3:
        oled.text("<", 115, 28)
        oled.text(" ", 5, 28)
        oled.text(" ", 52, 28)
        oled.text(" ", 52, 28)
    oled.show()  
    
def change_values(p_down, p_up, p_select, oled):
    global HOURS_ON, MINUTES_ON, HOURS_OFF, MINUTES_OFF, CURSOR, CHANGE_DATA
    
    print("i am in change values function")
    counts = 0
    condition = True
    while condition:
        down = p_down.value()
        up = p_up.value()
        select = p_select.value()
        
        if select:
            if CURSOR < 3:
                CURSOR += 1
                show_cursor(CURSOR, oled)
            elif CURSOR == 3:
                CURSOR = 0
                show_cursor(CURSOR, oled)

        else:
            if CURSOR == 0:
                # HOURS_ON
                if up:
                    if HOURS_ON < 23:
                        HOURS_ON += 1
                    else:
                        HOURS_ON = 0
                if down:
                    if HOURS_ON > 0:
                        HOURS_ON -= 1
                    else:
                        HOURS_ON = 23                   
            elif CURSOR == 1:
                # MINUTES_ON
                if up:
                    if MINUTES_ON < 59:
                        MINUTES_ON += 1
                    else:
                        MINUTES_ON = 0
                if down:
                    if MINUTES_ON > 0:
                        MINUTES_ON -= 1
                    else:
                        MINUTES_ON = 59 
                
            elif CURSOR == 2:
                # HOURS_OFF
                if up:
                    if HOURS_OFF < 23:
                        HOURS_OFF += 1
                    else:
                        HOURS_OFF = 0
                if down:
                    if HOURS_OFF > 0:
                        HOURS_OFF -= 1
                    else:
                        HOURS_OFF = 23
                
            elif CURSOR == 3:
                # MINUTES_OFF
                if up:
                    if MINUTES_OFF < 59:
                        MINUTES_OFF += 1
                    else:
                        MINUTES_OFF = 0
                if down:
                    if MINUTES_OFF > 0:
                        MINUTES_OFF -= 1
                    else:
                        MINUTES_OFF = 59 
        
        
        if counts > 10:
            condition = False
            
        if not down and not up and not select:
            counts += 1
        else:
            counts = 0
        
        show_on_display(oled)
        show_cursor(CURSOR, oled)
        utime.sleep(0.5)

def set_duty():
    global DUTY, MIN_DUTY, MAX_DUTY, CURRENT_ILUMINATION
       
    new_duty = MAX_DUTY - int(0.01*CURRENT_ILUMINATION*MAX_DUTY)
    print("new_duty: ", new_duty)
    duty_to_set = 0
    change = abs(DUTY - new_duty)
    if new_duty >= 0 and new_duty <= MAX_DUTY and change > 10:
        duty_to_set = new_duty
    else:
        duty_to_set = DUTY
        
    return duty_to_set

def LED_shining():
    global TIME_H, TIME_M, HOURS_ON, HOURS_OFF, MINUTES_ON, MINUTES_OFF
    
    shine = False
    
    if TIME_H == HOURS_ON and TIME_M >= MINUTES_ON:
        shine = True
    elif TIME_H > HOURS_ON and TIME_H < HOURS_OFF:
        shine = True
    elif TIME_H == HOURS_OFF and TIME_M <= MINUTES_OFF:
        shine = True
    else:
        shine = False
        
    return shine

#########################################
i2c_rtc = I2C(0, scl=Pin(13), sda=Pin(12), freq = 400000)
addr_R = I2C.scan(i2c_rtc)
print("clock ",addr_R)
for i in addr_R:
    print(hex(i))
rtc = DS1307(i2c_rtc)

year = 2022
month = 5
date = 1
day = 1
hour = 23
minute = 5
second = 0
    
now = (year, month, date, day, hour, minute, second, 0)
#rtc.datetime(now) # for changing time on RTC
print(rtc.datetime())

#OLED
i2c_O = I2C(1, scl=Pin(3), sda=Pin(2), freq= 400000)
utime.sleep(0.1)
print("oled", i2c_O.scan())
oled = SSD1306_I2C(WIDTH, HEIGHT, i2c_O)

########################################

# init Timers
soil = Timer()
soil.init(freq = 2, mode=Timer.PERIODIC, callback=get_humidity)

ilum = Timer()
ilum.init(freq = 2, mode=Timer.PERIODIC, callback=get_ilumination)

########################################

#buttons
p0 = Pin(22, Pin.IN)
p_down = Pin(20, Pin.IN, Pin.PULL_DOWN)
p_up = Pin(21, Pin.IN, Pin.PULL_DOWN)
p_select = Pin(19, Pin.IN, Pin.PULL_DOWN)

########################################
# PWM
pwm = PWM(Pin(16))
pwm.freq(200)
pwm.duty_u16(0) # default value
########################################

# main while loop
    
input_value = 1
while input_value:
    input_value = p0.value()
    (year, month, date, day, hour, minute, second, p1) = rtc.datetime()
    TIME_H = hour
    TIME_M = minute
    
    down = p_down.value()
    up = p_up.value()
    select = p_select.value()
    
    print("main_while_loop")
    
    new_duty = set_duty()
    
    led_shine = LED_shining()
    
    if led_shine:
        pwm.duty_u16(new_duty)
    else:
        pwm.duty_u16(0)
        
    
    
    if down or up or select:
        print("going to change values")
        change_values(p_down, p_up, p_select, oled)
        
    show_on_display(oled)
    utime.sleep(0.5)

oled.poweroff()
pwm.deinit()
ilum.deinit()
soil.deinit()


